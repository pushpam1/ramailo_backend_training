from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from .models import OrderItem, Product,Cart,CartItem
from .serializers import ProductSerializer, UserLoginSerializer, UserRegistrationSerializer, UserProfileSerializer,CartItemSerializer,CartSerializer,OrderSerializer
from django.contrib.auth import get_user_model, authenticate
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from rest_framework import permissions
from .errors import NotFoundError, ValidationError
from rest_framework.decorators import api_view

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            return []
        return [permission() for permission in self.permission_classes]
# from rest_framework.permissions import IsAdminUser

# class ProductViewSet(viewsets.ModelViewSet):
#     queryset = Product.objects.all()
#     serializer_class = ProductSerializer

#     def get_permissions(self):
#         if self.action in ['list', 'retrieve']:
#             permission_classes = [permissions.AllowAny]
#         else:  # For create, update, partial_update, destroy
#             permission_classes = [IsAdminUser]
#         return [permission() for permission in permission_classes]


class UserRegistrationView(generics.CreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserRegistrationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            refresh = RefreshToken.for_user(user)
            return Response({
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            }, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserLoginView(generics.GenericAPIView):
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data.get('email')
            password = serializer.validated_data.get('password')
            user = authenticate(request, email=email, password=password)
            if user:
                refresh = RefreshToken.for_user(user)
                return Response({
                    'refresh': str(refresh),
                    'access': str(refresh.access_token),
                })
            return Response({"error": "Invalid Credentials"}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserProfileView(generics.RetrieveUpdateAPIView):
    serializer_class = UserProfileSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        return self.request.user

@api_view(['GET'])
def trigger_not_found_error(request):
    raise NotFoundError("This is a custom Not Found error message.")

@api_view(['GET'])
def trigger_validation_error(request):
    raise ValidationError("This is a custom Validation error message.")


class CartView(generics.RetrieveUpdateAPIView):
    serializer_class = CartSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return Cart.objects.get_or_create(user=self.request.user)[0]

class AddToCartView(generics.CreateAPIView):
    serializer_class = CartItemSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        product = get_object_or_404(Product, pk=self.kwargs['product_id'])
        if product.stock <= 0:
            raise ValidationError("This product is out of stock.")
        cart = Cart.objects.get_or_create(user=self.request.user)[0]
        serializer.save(cart=cart, product=product)
        


class CartItemsView(generics.ListAPIView):
    """
    List all items in the user's cart.
    """
    serializer_class = CartItemSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user_cart = Cart.objects.get_or_create(user=self.request.user)[0]
        return CartItem.objects.filter(cart=user_cart)

class RemoveFromCartView(generics.DestroyAPIView):
    """
    Remove an item from the cart.
    """
    serializer_class = CartItemSerializer
    permission_classes = [IsAuthenticated]
    lookup_url_kwarg = 'item_id'

    def get_queryset(self):
        user_cart = Cart.objects.get_or_create(user=self.request.user)[0]
        return CartItem.objects.filter(cart=user_cart)

class ClearCartView(generics.DestroyAPIView):
    """
    Clear all items from the user's cart.
    """
    serializer_class = CartSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return Cart.objects.get_or_create(user=self.request.user)[0]

    def destroy(self, request, *args, **kwargs):
        cart = self.get_object()
        cart.items.all().delete()  # Delete all cart items
        return Response(status=status.HTTP_204_NO_CONTENT)


class PlaceOrderView(generics.CreateAPIView):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        cart = get_object_or_404(Cart, user=self.request.user)
        order = serializer.save(user=self.request.user, total_price=cart.get_total_price())
        
        # Create OrderItems for each item in the cart
        for item in cart.items.all():
            OrderItem.objects.create(order=order, product=item.product, quantity=item.quantity)
        
        # Clear the cart after placing the order
        cart.items.all().delete()
