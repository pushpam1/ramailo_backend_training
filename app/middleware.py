from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework import status
from .errors import NotFoundError, ValidationError

def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    # Handle specific custom exceptions
    if isinstance(exc, NotFoundError):
        return Response({"detail": exc.detail}, status=status.HTTP_404_NOT_FOUND)
    elif isinstance(exc, ValidationError):
        return Response({"detail": exc.detail}, status=status.HTTP_400_BAD_REQUEST)

    # For other exceptions, use the default handler
    return response
