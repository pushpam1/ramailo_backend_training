from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ProductViewSet
from .views import UserRegistrationView, UserLoginView,UserProfileView,trigger_not_found_error, trigger_validation_error,CartView,AddToCartView,CartItemsView, RemoveFromCartView, ClearCartView,PlaceOrderView

router = DefaultRouter()
router.register(r'products', ProductViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('register/', UserRegistrationView.as_view(), name='register'),
    path('login/', UserLoginView.as_view(), name='login'),
    path('profile/', UserProfileView.as_view(), name='user-profile'),
    path('trigger-not-found/', trigger_not_found_error, name='trigger-not-found'),
    path('trigger-validation/', trigger_validation_error, name='trigger-validation'),
    path('cart/', CartView.as_view(), name='cart'),
    path('cart/add/<int:product_id>/', AddToCartView.as_view(), name='add-to-cart'),
    path('cart/items/', CartItemsView.as_view(), name='cart-items'),
    path('cart/remove/<int:item_id>/', RemoveFromCartView.as_view(), name='remove-from-cart'),
    path('cart/clear/', ClearCartView.as_view(), name='clear-cart'),
    path('order/place/', PlaceOrderView.as_view(), name='place-order'),
]
