class NotFoundError(Exception):
    def __init__(self, detail="Resource not found"):
        self.detail = detail

class ValidationError(Exception):
    def __init__(self, detail="Invalid input"):
        self.detail = detail
